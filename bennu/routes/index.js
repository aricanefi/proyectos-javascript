  var express = require('express');
  var router = express.Router();


  // base de datos Mongo  coneccion
  var mongoose = require('mongoose');
  mongoose.connect('mongodb://localhost/bennu');

  var db = mongoose.connection;
  db.on('error', console.error.bind(console, 'connection error:'));
  db.once('open', function() {
    // we're connected!
  });

  // inicializacion de Schema en mongodb
  var usuarioSchema = mongoose.Schema({

      nombre: String,
      apellido: String,
      usuario: String,
      password: String,
      usuario:String,
      genero: String,
      estadoCivil: String

  });

  // iniciando db
  var Usuariodb = mongoose.model('Usuariodb', usuarioSchema);





// ingreso a la base de datos desde el formulario
  router.post('/usuario',function(req, res, next){

        var id = req.body.id;
        var nombre = req.body.nombre;
        var apellido = req.body.apellido;
        var usuario = req.body.usuario;
        var password = req.body.password;
        var genero = req.body.genero;
        var estadoCivil = req.body.estadoCivil;

        var usuariomongo = new Usuariodb({
              nombre: nombre,
              apellido: apellido,
              usuario: usuario,
              password: password,
              genero:  genero,
              estadoCivil: estadoCivil
            });


        if (!id){

  usuariomongo.save(function (err,usuario ){
        if (err) return console.error(err);
        console.log('ingreso de datos satisfactorio');
        res.redirect('/');

      });
}else  {


  //modificar
    Usuariodb.findByIdAndUpdate(id,{ $set:{nombre: nombre,apellido: apellido,
      usuario:usuario,password:password,genero:genero,estadoCivil:estadoCivil}},function (err, usuario){
      if (err) return console.error(err);
      res.redirect('/');
    });
  }
});




  /*Rutas post borrar */
  router.get('/delete/:id', function(req, res, next) {
      var id = req.params.id;
      console.log(id);
      Usuariodb.findByIdAndRemove(id,function(err,removed){
          if (err) return console.error(err);

            res.redirect('/');
    });
  });


  /*Rutas post llegar al formulario vacio ( usuario nuevo)*/
  router.get('/usuario', function(req, res, next) {
    res.render('formulario',{usuariomod : {}});
  });

  // ruta para modificar datos
  router.get('/usuario/:id', function(req, res, next) {

    var id = req.params.id;
    Usuariodb.findById(id,function(err,usuarios){
          if (err) return console.error(err);
         res.render('formulario',{usuariomod : usuarios})
    });
           //res.render('formulario');
});


  router.get('/', function(req, res, next) {
      Usuariodb.find({},function(err,usuarios){
          if (err) return console.error(err);
          res.render('index', { usuariolist: usuarios });
      });
    });

  module.exports = router;
