  var FormViews = Backbone.View.extend({
        el: "#form",
        template: _.template($('#formtpl').html()),
        events:{
            "submit #Formulario": "save"
        },
        initialize: function(){
          this.render();

        },
        render: function(){
          this.$el.html(this.template());

        },
        save: function(e){
            e.preventDefault();
            var nombre =     $('#nombre').val();
            var apellido =   $('#apellido').val();
            var usuario =    $('#usuario').val();
            var password =   $('#password').val();
            var genero =     $('#genero').val();
            var estadoCivil= $('#estadoCivil').val();

            var usuario = new UsuarioModel({
                 nombre : nombre,
                 apellido : apellido,
                 usuario : usuario,
                 password : password,
                 genero : genero,
                 estadoCivil : estadoCivil
              });

              var collection = this.collection;
                    usuario.save({},{
                       success: function(){
                       collection.fetch();
               }
            });

        }
    });
