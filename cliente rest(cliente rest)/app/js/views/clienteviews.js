var UsuarioView =  Backbone.View.extend({
  el: "#usuarios",

  events: {
        'click .eliminar'  : 'removeCliente',
        'click .modificar' : 'updateCliente'
  },

  initialize: function() {

  //  this.listenTo(this.collection, 'reset', this.render);
    this.collection.on('change',this.render,this);
    this.collection.on('update',this.render,this);
    this.collection.on("reset",this.render,this);
    this.collection.on("add", this.render.this);
    this.collection.on("remove", this.render, this);
    this.collection.on("destroy", this.render,this);
    this.collection.on('sync', this.render, this);
    this.collection.fetch();
    this.render();


   },

   render: function() {
            this.$el.empty();
            this.collection.each(function(usuarios){
            var template =  _.template($('#clientetpl').html());
            var vista = template(usuarios.toJSON());
            this.$el.append(vista);

    },this);

   },

   removeCliente: function(evento){
          var id = $(evento.target).data("id")
          var model = this.collection.get(id);
          this.collection.remove(id);
          model.destroy();

   },
   updateCliente: function(evento){
           var id = $(evento.target).data("id");
           var model = this.collection.get(id);
           this.$el.html(this.template(this.model.toJSON()));
           return this;

  }

});
