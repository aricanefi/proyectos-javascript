require.config({

// rutas de las librerias a cargar
paths: {

      jQuery: '/js/libs/jquery-1.12.0.min',
      Underscore: '/js/libs/underscore-min',
      Backbone: '/js/libs/backbone-min',
      text: '/js/libs/text',
      templates: '../templates'

},

// usando shim para cargar librerias como jquery
shim: {

      'Backbone': ['Underscore', 'jQuery'],
      'SocialNet': ['Backbone']
}
});

// inicializando las librerias
require(['SocialNet'], function(SocialNet) {
SocialNet.initialize();
});
