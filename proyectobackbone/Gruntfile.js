module.exports = function(grunt){

require('load-grunt-task')(grunt);


grunt.initConfig({


// tarea clean
clean: {
      dist: ['dist']
    },
    jst: {
      compile: {
        files: {
          'dist/assets/js/templates.js' : ['public/js/templates/*.ejs']
        }
      }
    },

// tarea uglify
    uglify: {
      vendor: {
        files: {
          'dist/assets/js/vendor.min.js' :
          ['bower_components/jquery/dist/jquery.min.js',
          'bower_components/underscore/underscore-min.js',
          'bower_components/backbone/backbone-min.js']
        }
      }
    },


// tarea copy
    copy: {
      main: {
        files:
        [{
            expand: true,
            cwd: 'public/js',
            src: ['{,*/}*.*'],
            dest: 'dist/assets/js'
        },
        {
            expand: true,
            flatten: true,
            cwd: 'public',
            src: ['*.*'],
            dest: 'dist/',
            filter: 'isFile'
        }]
      }
    }


});



grunt.registerTask('createDefaultTemplate', function (){

    grunt.file.write('dist/assets/js/templates.js', 'this.JST = this.JST || {};');

});

    grunt.registerTask('default', ['clean', 'createDefaultTemplate', 'jst', 'uglify']);

}
